(function ($, Drupal) {

  Drupal.behaviors.cemvicios = {
    attach: function(context, settings) {

      $('#fullpage').hide();
      $('#videos').hide();

      $(window).resize(function(){location.reload();});

      $(window).load(function(){

        $('#overlay').fadeOut();
        $('#fullpage').fadeIn(1000);
        
      });
      


      $(document).ready(function() {


          new WOW().init();

          $(".pratos").backstretch([
            "sites/default/files/_mg_03170.jpg",
            "sites/default/files/_mg_03195.jpg"
          ], {duration: 4000, fade: 750}); //backstretch pratos

          $(".imprensa").backstretch([
            "sites/default/files/_mg_03170.jpg",
            "sites/default/files/_mg_03195.jpg"
          ], {duration: 4000, fade: 750}); //backstretch imprensa


          $(window).setBreakpoints({

              
              breakpoints: [
                  1200
              ] 
          }); //setbreakpoints


          $(window).bind('enterBreakpoint1200',function() {

            $(function() {
              var BV = new $.BigVideo();
              
              BV.init();

              BV.show($('#videos').attr('data-video'),{ambient:true});
              
            });

            $('#fullpage').fullpage({

              keyboardScrolling: true,
              menu: '#menu',



              anchors: ['01', '02', '03', '04', '05', '06', '07', '08', '09'],

              afterLoad: function(anchorLink, index){

                  if(anchorLink == '01'){
                      

                      $(".nav").animate({ opacity: 0 }, 100);

                  }

                  //using anchorLink
                  if(anchorLink == '02'){
                      

                      $(".nav").animate({ opacity: 1 }, 200);

                  }

                  if(anchorLink == '03'){
                      

                      $(".nav").animate({ opacity: 1 }, 200);

                  }

                  if(anchorLink == '04'){
                      

                      $(".nav").animate({ opacity: 1 }, 200);

                  }


                  if(anchorLink == '05'){
                      

                      $(".nav").animate({ opacity: 1 }, 200);

                  }

                  if(anchorLink == '06'){
                      

                      $(".nav").animate({ opacity: 1 }, 200);

                  }

                  if(anchorLink == '07'){
                      

                      $(".nav").animate({ opacity: 1 }, 200);

                  }

                  if(anchorLink == '09'){
                      

                      $(".nav").animate({ opacity: 1 }, 200);

                  }

                  // $('.principal').videoBG({
                  //   position:"relative",
                  //   zIndex:0,
                  //   mp4:'sites/all/themes/100vicios/assets/video.mp4',
                  //   ogv:'sites/all/themes/100vicios/assets/video.ogv',
                  //   webm:'sites/all/themes/100vicios/assets/video.webm',
                  //   poster:'sites/all/themes/100vicios/images/chef.jpg',
                  //   opacity:1,
                  //   fullscreen:true
                  // }); //videobg

              } //afterload


            }); //fullpage


            // $(".pratos").backstretch([
            //   "sites/default/files/_mg_03170.jpg",
            //   "sites/default/files/_mg_03195.jpg"
            // ], {duration: 4000, fade: 750}); //backstretch pratos

            // $(".imprensa").backstretch([
            //   "sites/default/files/_mg_03170.jpg",
            //   "sites/default/files/_mg_03195.jpg"
            // ], {duration: 4000, fade: 750}); //backstretch imprensa
              
          }); //breakpoint 1200

          
      });//document ready


    }
  };

})(jQuery, Drupal);
