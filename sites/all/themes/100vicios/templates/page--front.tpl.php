<div id="overlay">
     <img src="sites/all/themes/100vicios/images/loading.gif" alt="Loading" />
     <p>A carregar...</p>
     
</div>


<div id="fullpage" class="show-for-large-only">

    <div class="section entrada" data-anchor="entrada">

      <div class="logo">
        
        <img src="sites/all/themes/100vicios/images/logo_100vicios.png">

        <h1>Bem-vindo.</h1>
        <h3>scroll</h3>

      </div><!--  LOGO -->

        <div class="seta_branca">
          <a href="#02"><img src="sites/all/themes/100vicios/images/seta_branca.png"></a>
        </div><!--  SETA BRANCA -->

        <div class="seta_dourada">
          <a href="#02"><img src="sites/all/themes/100vicios/images/seta_dourada.png"></a>
        </div><!--  SETA DOURADA -->

    </div><!--  ENTRADA -->





    <div class="section principal" class="toSlide" data-index="02">

      <article class="video" id="videos" data-video="sites/all/themes/100vicios/assets/video.mp4"> </article>

    </div><!--  PRINCIPAL -->






    <div class="section restaurante toSlide" data-index="03">

      <section class="descricao wow fadeIn">

        <h2>100 Vícios</h2>
        
        <article>

          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pharetra quam sit amet porta posuere. Praesent vel lectus a lorem volutpat venenatis. Nullam lobortis sed justo ac congue. Curabitur laoreet metus nibh, ac sagittis risus laoreet rutrum. Nullam congue rutrum eleifend. Fusce mollis urna in nisl mattis, ultrices posuere metus vulputate. Sed scelerisque aliquam quam commodo rutrum. Fusce aliquet adipiscing sem at egestas.</p>

        </article><!-- article -->

      </section><!-- descricao -->

    </div><!--  restaurante -->
    




    <div class="section chef toSlide" data-index="04">

      <section class="descricao wow fadeIn">

        <h2>Chef António Alexandre</h2>
        
        <article>
          
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pharetra quam sit amet porta posuere. Praesent vel lectus a lorem volutpat venenatis. Nullam lobortis sed justo ac congue. Curabitur laoreet metus nibh, ac sagittis risus laoreet rutrum. Nullam congue rutrum eleifend. Fusce mollis urna in nisl mattis, ultrices posuere metus vulputate. Sed scelerisque aliquam quam commodo rutrum. Fusce aliquet adipiscing sem at egestas.</p>

        </article><!-- article -->

      </section><!-- descricao -->

    </div><!--  chef -->


    <div class="section pratos toSlide" data-index="05">

    </div><!--  pratos -->


    <div class="section imprensa toSlide" data-index="06">

    </div><!--  imprensa -->


    <div class="section reservas toSlide" data-index="07">

        <?php print render($page['reservas']); ?>

    </div><!--  reservas -->


    <div class="section contactos toSlide" data-index="08">

      <?php print render($page['mapa']); ?>

      <div class="contactos_dados">
        
        <h1>Contactos</h1>

        <p>
          Travessa Alfarrobeira 2 <br>
          2750-285 Cascais <br>
          <br>
          +351 214 867 230 <br>
          <br>
          facebook.com/100vicios<br>
          info@100vicios.pt
        </p>

      </div><!-- contactos_dados -->

    </div><!--  contactos -->


    <div class="nav">

      <nav>

        <ul id="menu">

          <li data-menuanchor="02"><a href="#02">início</a></li>
          <li data-menuanchor="03"><a href="#03">restaurante</a></li>
          <li data-menuanchor="04"><a href="#04">o chef</a></li>
          <li data-menuanchor="05"><a href="#05">pratos</a></li>
          <li data-menuanchor="01" class="cemvicios"><a href="#01"><img src="sites/all/themes/100vicios/images/100vicios.png"></a></li>
          <li data-menuanchor="06"><a href="#06">imprensa</a></li>
          <li data-menuanchor="07"><a href="#07">reservas</a></li>
          <li data-menuanchor="08"><a href="#08">contactos</a></li>
          <li><a href="#">pt | en</a></li>

        </ul>

      </nav>

    </div><!-- nav -->

    
</div><!--  Fullpage -->